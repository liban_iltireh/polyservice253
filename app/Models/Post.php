<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Post extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $translatable = ['title', 'body', 'slug'];
}

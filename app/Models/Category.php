<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{

    use SoftDeletes;
    use Translatable;
    protected $translatable = ['title', 'description'];

    public function services()
    {
        return $this->HasMany(Service::class);
    }
}

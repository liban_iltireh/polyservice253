<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;


class Article extends Model
{
    protected $casts = ['date' => 'date'];
    use SoftDeletes;
    use Translatable;
    use Resizable;
    protected $translatable = ['title', 'body', 'slug'];
}

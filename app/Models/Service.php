<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;


class Service extends Model
{
    use SoftDeletes;
    use Translatable;
    protected $translatable = ['title', 'description'];

    public function categories()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function sellers()
    {
        return $this->belongsToMany(Seller::class);
    }
}

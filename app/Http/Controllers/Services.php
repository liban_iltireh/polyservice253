<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Service;
use Illuminate\Http\Request;

class Services extends Controller
{
    public function index()
    {
        $title = 'Services';
        $serv = Service::with('categories')->orderBy('title')->get();

        return view('pages.services', compact('serv', 'title'));
    }
    public function show($slug)
    {
        $CatServ = Category::with('services')->findOrFail($slug);
        $title = $CatServ->getTranslatedAttribute('title');

        return view('pages.services_category', compact('CatServ', 'title'));
    }
    public function getSellers($slug)
    {
        $service = Service::with('sellers')->findOrFail($slug);
        $title = $service->getTranslatedAttribute('title');

        return view('pages.service_sellers', compact('service', 'title'));
    }
}

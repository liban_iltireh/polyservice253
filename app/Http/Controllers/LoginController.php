<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class LoginController extends Controller
{
    // Login

    public function authenticate(Request $request)
    {
        $this->validate($request, [
           'email' => 'required|email',
           'password' => 'required'
        ]);
        $credentials = $request->only('email', 'password');

        if ($request->path() === 'providers/login') {
            $credentials['is_active'] = 1;
        }

        if ($request->path() === 'customers/login' && Auth::guard('customer')->attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }elseif ($request->path() === 'providers/login' && Auth::guard('seller')->attempt($credentials)){
            $request->session()->regenerate();

            return redirect()->intended('/');
        }

        return back()->withErrors([
            'login' => 'The provided credentials do not match our records.',
        ]);
    }

    // Logout

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function store(Request $request) {

        if ($request->path() === 'customers/register') {
            $register = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:customers',
                'password' => 'required|string|min:8|confirmed',
                'tel' => 'required|numeric',
                'adresse' => 'required|string',
                'region' => ['required', Rule::in(['Djibouti', 'Dikhil', 'Arta', 'Ali Sabieh', 'Tadjourah', 'Obock'])],
                'genre' => ['required', Rule::in(['Homme', 'Femme'])]
            ]);
            $register['password'] = Hash::make($register['password']);

            $customer = Customer::create($register);
        }
        elseif ($request->path() === 'providers/register'){

            $register = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|email|unique:sellers',
                'password' => 'required||string|min:8|confirmed',
                'tel' => 'required|numeric',
                'adresse' => 'required|string',
                'region' => ['required', Rule::in(['Djibouti', 'Dikhil', 'Arta', 'Ali Sabieh', 'Tadjourah', 'Obock'])],
                'services' => 'required|array|exists:services,id'
            ]);

            $register['password'] = Hash::make($register['password']);

            $seller = Seller::create($register);

            $seller->services()->attach($register['services']);
        }
        $request->session()->flash('stored', true);
        return back();
    }
}

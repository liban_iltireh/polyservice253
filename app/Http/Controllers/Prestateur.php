<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


class Prestateur extends Controller
{
    public function index(){
        $title = 'Polyservices253';
        return view('pages.prestateur', compact('title'));
    }

    public function edit(){
        $title = 'Profil';
        $user = Seller::with('services')->findOrFail(auth()->id());
        return view('pages.prestateur-edit', compact('title', 'user'));
    }

    public function update(Request $request)
    {
        $register = $request->validate([
            'name' => 'required|string|max:255',
            'tel' => 'required|numeric',
            'adresse' => 'required|string',
            'region' => ['required', Rule::in(['Djibouti', 'Dikhil', 'Arta', 'Ali Sabieh', 'Tadjourah', 'Obock'])],
            'services' => 'required|array|exists:services,id'
        ]);

        Seller::where('id', auth()->id())->update($request->except(['services', '_token']));
        $seller = Seller::find(auth()->id());
        $seller->services()->sync($register['services']);

        $request->session()->flash('stored', true);
        return redirect('/providers/edit');
    }
}

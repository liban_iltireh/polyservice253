<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class Blog extends Controller
{
    public function index($slug){
        $art = Article::whereTranslation('slug', $slug)->firstOrFail();
        $title = $art->getTranslatedAttribute('slug');
        return view('pages.detailarticle', compact('art', 'title'));
    }
    public function allArticles(){
        $title = 'Blog';
        $cat = Category::orderBy('title', 'asc')->get();
        $articles = Article::orderBy('date', 'desc')->paginate(10);
        return view('pages.articles', compact('title', 'articles', 'cat'));
    }
}

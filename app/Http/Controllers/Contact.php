<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Contact extends Controller
{
    public function index(){
        $title = 'Polyservices253';

        return view('pages.contact', compact('title'));
    }
}

<?php

namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Category;
use App\Models\Review;
use App\Models\Slider;
use Illuminate\Support\Facades\App;


class Home extends Controller
{
    public function index()
    {
        App::setLocale(session('locale'));

        $title = 'Polyservices253';
        $cat = Category::orderBy('title', 'asc')->get()->chunk(6);
        $arts = Article::latest('date')->take(3)->get();
        $reviews = Review::all();
        $slider = Slider::latest()->take(5)->get();
        return view('pages.index', compact('cat', 'reviews', 'arts', 'title', 'slider'));
    }
}

-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: polydb
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `articles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `articles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `title`, `body`, `image`, `date`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES (1,'7 indicateurs de performance des ventes à suivre dans toute entreprise','<p><span style=\"color: #031941; font-family: Mada, sans-serif; font-size: 16.8px; letter-spacing: 0.5px;\">Voici les 7 indicateurs de performance des ventes essentiels que toute entreprise doit suivre pour &ecirc;tre efficace. Notez que cette liste n&rsquo;est pas exhaustive : d&rsquo;autres indicateurs devraient s&rsquo;y ajouter, selon les besoins et les objectifs sp&eacute;cifiques de l&rsquo;entreprise.</span></p>','articles/February2021/sZFNiEVZqadDQxNVwOoE.png','2021-02-28 00:00:00','2021-02-28 06:17:39','2021-02-28 06:18:04',NULL,'7-indicateurs-de-performance-des-ventes-a-suivre-dans-toute-entreprise');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `image`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'Bien-Etre','categories/February2021/aAClmjuBvN9HqehLBE7F.png','Ton bien-être','2021-02-16 02:38:23','2021-02-16 02:38:23',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_rows` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(29,5,'id','text','Id',1,0,0,0,0,0,'{}',1),(30,5,'title','text','Titre',1,1,1,1,1,1,'{}',2),(31,5,'image','image','Image',0,1,1,1,1,1,'{}',3),(32,5,'description','text_area','Description',0,1,1,1,1,1,'{}',4),(33,5,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',5),(34,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(35,5,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',7),(36,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(37,6,'title','text','Titre',0,1,1,1,1,1,'{}',2),(38,6,'image','image','Image',0,1,1,1,1,1,'{}',3),(39,6,'description','text_area','Description',0,1,1,1,1,1,'{}',4),(40,6,'category_id','hidden','Catégorie',1,1,1,1,1,1,'{}',5),(41,6,'created_at','timestamp','Créer le',0,1,1,0,0,0,'{}',6),(42,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(43,6,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',8),(44,6,'service_belongsto_category_relationship','relationship','Catégorie',0,1,1,1,1,1,'{\"model\":\"App\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',9),(53,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(54,8,'name','text','Name',1,1,1,1,1,1,'{}',2),(55,8,'message','text_area','Message',1,1,1,1,1,1,'{}',3),(56,8,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',4),(57,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(58,8,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',6),(59,9,'id','text','Id',1,0,0,0,0,0,'{}',1),(60,9,'title','text','Titre',1,1,1,1,1,1,'{}',2),(61,9,'body','rich_text_box','Contenue',0,1,1,1,1,1,'{}',3),(62,9,'image','image','Image',1,1,1,1,1,1,'{}',4),(63,9,'date','date','Date',1,1,1,1,1,1,'{}',5),(64,9,'created_at','timestamp','Créer le',0,0,0,0,0,0,'{}',6),(65,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(66,9,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',8),(67,9,'slug','hidden','Slug',0,1,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',9);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController','',1,0,NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(5,'categories','categories','Catégorie','Catégories','voyager-archive','App\\Models\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2021-02-16 02:36:04','2021-02-16 02:36:04'),(6,'services','services','Service','Services','voyager-shop','App\\Models\\Service',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2021-02-16 06:17:02','2021-02-16 06:20:08'),(8,'reviews','reviews','Témoignage','Témoignages','voyager-bubble-hear','App\\Models\\Review',NULL,NULL,NULL,1,0,'{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2021-02-27 14:21:40','2021-02-27 14:21:40'),(9,'articles','articles','Article','Articles','voyager-news','App\\Models\\Article',NULL,NULL,NULL,1,0,'{\"order_column\":\"date\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}','2021-02-28 06:12:53','2021-02-28 06:17:58');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES (1,1,'Accueil','','_self','voyager-boat','#000000',NULL,1,'2021-02-15 06:14:02','2021-02-15 06:31:12','voyager.dashboard','null'),(2,1,'Media','','_self','voyager-images',NULL,NULL,8,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.media.index',NULL),(3,1,'Utilisateurs','','_self','voyager-person','#000000',NULL,7,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.users.index','null'),(4,1,'Rôles','','_self','voyager-lock','#000000',NULL,6,'2021-02-15 06:14:02','2021-02-27 14:22:13','voyager.roles.index','null'),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,12,'2021-02-15 06:14:02','2021-02-27 14:22:09',NULL,NULL),(6,1,'Générateur des Menus','','_self','voyager-list','#000000',NULL,10,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.menus.index','null'),(7,1,'Database','','_self','voyager-data',NULL,NULL,11,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,1,'2021-02-15 06:14:02','2021-02-15 06:16:28','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,NULL,9,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,13,'2021-02-15 06:14:02','2021-02-27 14:22:09','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,2,'2021-02-15 06:14:02','2021-02-15 06:16:31','voyager.hooks',NULL),(13,1,'Catégories','','_self','voyager-archive','#000000',NULL,4,'2021-02-16 02:36:04','2021-02-27 13:14:40','voyager.categories.index','null'),(14,1,'Services','','_self','voyager-shop',NULL,NULL,3,'2021-02-16 06:17:02','2021-02-27 13:14:44','voyager.services.index',NULL),(16,1,'Témoignages','','_self','voyager-bubble-hear',NULL,NULL,5,'2021-02-27 14:21:40','2021-02-27 14:22:13','voyager.reviews.index',NULL),(17,1,'Articles','','_self','voyager-news',NULL,NULL,2,'2021-02-28 06:12:53','2021-02-28 06:13:02','voyager.articles.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES (1,'admin','2021-02-15 06:14:02','2021-02-15 06:14:02');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES (1,1),(1,2),(2,1),(3,1),(4,1),(5,1),(6,1),(6,2),(7,1),(7,2),(8,1),(8,2),(9,1),(9,2),(10,1),(10,2),(11,1),(11,2),(12,1),(12,2),(13,1),(13,2),(14,1),(14,2),(15,1),(15,2),(16,1),(16,2),(17,1),(17,2),(18,1),(18,2),(19,1),(19,2),(20,1),(20,2),(21,1),(21,2),(22,1),(22,2),(23,1),(23,2),(24,1),(24,2),(25,1),(25,2),(26,1),(32,1),(32,2),(33,1),(33,2),(34,1),(34,2),(35,1),(35,2),(36,1),(36,2),(37,1),(37,2),(38,1),(38,2),(39,1),(39,2),(40,1),(40,2),(41,1),(41,2),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES (1,'browse_admin',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(2,'browse_bread',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(3,'browse_database',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(4,'browse_media',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(5,'browse_compass',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(6,'browse_menus','menus','2021-02-15 06:14:02','2021-02-15 06:14:02'),(7,'read_menus','menus','2021-02-15 06:14:02','2021-02-15 06:14:02'),(8,'edit_menus','menus','2021-02-15 06:14:02','2021-02-15 06:14:02'),(9,'add_menus','menus','2021-02-15 06:14:02','2021-02-15 06:14:02'),(10,'delete_menus','menus','2021-02-15 06:14:02','2021-02-15 06:14:02'),(11,'browse_roles','roles','2021-02-15 06:14:02','2021-02-15 06:14:02'),(12,'read_roles','roles','2021-02-15 06:14:02','2021-02-15 06:14:02'),(13,'edit_roles','roles','2021-02-15 06:14:02','2021-02-15 06:14:02'),(14,'add_roles','roles','2021-02-15 06:14:02','2021-02-15 06:14:02'),(15,'delete_roles','roles','2021-02-15 06:14:02','2021-02-15 06:14:02'),(16,'browse_users','users','2021-02-15 06:14:02','2021-02-15 06:14:02'),(17,'read_users','users','2021-02-15 06:14:02','2021-02-15 06:14:02'),(18,'edit_users','users','2021-02-15 06:14:02','2021-02-15 06:14:02'),(19,'add_users','users','2021-02-15 06:14:02','2021-02-15 06:14:02'),(20,'delete_users','users','2021-02-15 06:14:02','2021-02-15 06:14:02'),(21,'browse_settings','settings','2021-02-15 06:14:02','2021-02-15 06:14:02'),(22,'read_settings','settings','2021-02-15 06:14:02','2021-02-15 06:14:02'),(23,'edit_settings','settings','2021-02-15 06:14:02','2021-02-15 06:14:02'),(24,'add_settings','settings','2021-02-15 06:14:02','2021-02-15 06:14:02'),(25,'delete_settings','settings','2021-02-15 06:14:02','2021-02-15 06:14:02'),(26,'browse_hooks',NULL,'2021-02-15 06:14:02','2021-02-15 06:14:02'),(32,'browse_categories','categories','2021-02-16 02:36:04','2021-02-16 02:36:04'),(33,'read_categories','categories','2021-02-16 02:36:04','2021-02-16 02:36:04'),(34,'edit_categories','categories','2021-02-16 02:36:04','2021-02-16 02:36:04'),(35,'add_categories','categories','2021-02-16 02:36:04','2021-02-16 02:36:04'),(36,'delete_categories','categories','2021-02-16 02:36:04','2021-02-16 02:36:04'),(37,'browse_services','services','2021-02-16 06:17:02','2021-02-16 06:17:02'),(38,'read_services','services','2021-02-16 06:17:02','2021-02-16 06:17:02'),(39,'edit_services','services','2021-02-16 06:17:02','2021-02-16 06:17:02'),(40,'add_services','services','2021-02-16 06:17:02','2021-02-16 06:17:02'),(41,'delete_services','services','2021-02-16 06:17:02','2021-02-16 06:17:02'),(47,'browse_reviews','reviews','2021-02-27 14:21:40','2021-02-27 14:21:40'),(48,'read_reviews','reviews','2021-02-27 14:21:40','2021-02-27 14:21:40'),(49,'edit_reviews','reviews','2021-02-27 14:21:40','2021-02-27 14:21:40'),(50,'add_reviews','reviews','2021-02-27 14:21:40','2021-02-27 14:21:40'),(51,'delete_reviews','reviews','2021-02-27 14:21:40','2021-02-27 14:21:40'),(52,'browse_articles','articles','2021-02-28 06:12:53','2021-02-28 06:12:53'),(53,'read_articles','articles','2021-02-28 06:12:53','2021-02-28 06:12:53'),(54,'edit_articles','articles','2021-02-28 06:12:53','2021-02-28 06:12:53'),(55,'add_articles','articles','2021-02-28 06:12:53','2021-02-28 06:12:53'),(56,'delete_articles','articles','2021-02-28 06:12:53','2021-02-28 06:12:53');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `name`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'ali mohamed','Super SIte web','2021-02-27 14:31:03','2021-02-27 14:31:03',NULL),(2,'liban iltireh','Très bien service.','2021-02-27 14:31:35','2021-02-27 14:31:35',NULL);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES (1,'superadmin','Super Administrator','2021-02-15 06:14:02','2021-02-16 02:49:59'),(2,'admin','Administrator','2021-02-15 06:14:02','2021-02-16 02:50:43');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `services` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` (`id`, `title`, `image`, `description`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES (1,'Coiffeur','services/February2021/3vaB8s3OQgyca5nHbKP5.png','ton coiffeur',1,'2021-02-16 06:20:32','2021-02-16 06:20:32',NULL);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','settings/February2021/OF6N1jm5PjeYZmNzHS7N.jpg','','image',5,'Admin'),(6,'admin.title','Admin Title','PolyService253','','text',1,'Admin'),(7,'admin.description','Admin Description','Bienvenue au panneau d\'administration','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','settings/February2021/05EpmwuK8azyXSnhNgmX.png','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES (1,'categories','title',1,'en','Well being','2021-02-16 02:47:21','2021-02-16 02:47:21'),(2,'categories','description',1,'en','Your Well being','2021-02-16 02:47:21','2021-02-16 02:47:21'),(3,'menu_items','title',1,'en','Dashboard','2021-02-16 02:47:53','2021-02-16 02:47:53'),(4,'menu_items','title',13,'en','Categories','2021-02-16 02:48:14','2021-02-16 02:48:14'),(5,'menu_items','title',4,'en','Roles','2021-02-16 02:48:23','2021-02-16 02:48:23'),(6,'menu_items','title',3,'en','Users','2021-02-16 02:48:35','2021-02-16 02:48:35'),(7,'menu_items','title',6,'en','Menu Builder','2021-02-16 02:52:00','2021-02-16 02:52:00'),(8,'data_rows','display_name',36,'en','Id','2021-02-16 06:17:19','2021-02-16 06:17:19'),(9,'data_rows','display_name',37,'en','Titre','2021-02-16 06:17:19','2021-02-16 06:17:19'),(10,'data_rows','display_name',38,'en','Image','2021-02-16 06:17:19','2021-02-16 06:17:19'),(11,'data_rows','display_name',39,'en','Description','2021-02-16 06:17:19','2021-02-16 06:17:19'),(12,'data_rows','display_name',40,'en','Catégorie','2021-02-16 06:17:19','2021-02-16 06:17:19'),(13,'data_rows','display_name',41,'en','Créer le','2021-02-16 06:17:19','2021-02-16 06:17:19'),(14,'data_rows','display_name',42,'en','Updated At','2021-02-16 06:17:19','2021-02-16 06:17:19'),(15,'data_rows','display_name',43,'en','Deleted At','2021-02-16 06:17:19','2021-02-16 06:17:19'),(16,'data_types','display_name_singular',6,'en','Service','2021-02-16 06:17:19','2021-02-16 06:17:19'),(17,'data_types','display_name_plural',6,'en','Services','2021-02-16 06:17:19','2021-02-16 06:17:19'),(18,'data_rows','display_name',44,'en','categories','2021-02-16 06:18:06','2021-02-16 06:18:06'),(19,'data_rows','display_name',59,'en','Id','2021-02-28 06:17:58','2021-02-28 06:17:58'),(20,'data_rows','display_name',60,'en','Titre','2021-02-28 06:17:58','2021-02-28 06:17:58'),(21,'data_rows','display_name',61,'en','Contenue','2021-02-28 06:17:58','2021-02-28 06:17:58'),(22,'data_rows','display_name',62,'en','Image','2021-02-28 06:17:58','2021-02-28 06:17:58'),(23,'data_rows','display_name',63,'en','Date','2021-02-28 06:17:58','2021-02-28 06:17:58'),(24,'data_rows','display_name',64,'en','Créer le','2021-02-28 06:17:58','2021-02-28 06:17:58'),(25,'data_rows','display_name',65,'en','Updated At','2021-02-28 06:17:58','2021-02-28 06:17:58'),(26,'data_rows','display_name',66,'en','Deleted At','2021-02-28 06:17:58','2021-02-28 06:17:58'),(27,'data_rows','display_name',67,'en','Slug','2021-02-28 06:17:58','2021-02-28 06:17:58'),(28,'data_types','display_name_singular',9,'en','Article','2021-02-28 06:17:58','2021-02-28 06:17:58'),(29,'data_types','display_name_plural',9,'en','Articles','2021-02-28 06:17:58','2021-02-28 06:17:58');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES (2,2);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES (1,1,'admin','admin@admin.com','users/default.png',NULL,'$2y$10$qcd7funfgaGYBXTXFiM9N.0K37DodviorC7b51c2YVnLNjvntZzyG',NULL,'{\"locale\":\"fr\"}','2021-02-15 06:14:34','2021-02-16 02:49:11'),(2,2,'Admin','test@test.com','users/default.png',NULL,'$2y$10$TDXHDJr0Yc0M2/VI/7/ERe9iB30RnTtJUSaj..SUhko/tqSlSZYUu',NULL,'{\"locale\":\"fr\"}','2021-02-16 02:51:11','2021-02-16 02:51:11');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-02  8:28:39

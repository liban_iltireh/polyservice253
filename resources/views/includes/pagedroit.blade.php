<div class="col-lg-4 sidebar">
    <div class="single-slidebar">
        <h4>@lang('front.prestateur_local')</h4>
        <ul class="cat-list">
            <li><a class="justify-content-between d-flex" href="category.html"><p>New York</p><span>37</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Park Montana</p><span>57</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Atlanta</p><span>33</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Arizona</p><span>36</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Florida</p><span>47</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Rocky Beach</p><span>27</span></a></li>
            <li><a class="justify-content-between d-flex" href="category.html"><p>Chicago</p><span>17</span></a></li>
        </ul>
    </div>

    <div class="single-slidebar">
        <h4>@lang('front.meilleure_prestateur')</h4>
        <div class="active-relatedjob-carusel">
            <div class="single-rated">
                <img class="img-fluid" src="img/r1.jpg" alt="">
                <a href="single.html"><h4>Creative Art Designer</h4></a>
                <h6>Premium Labels Limited</h6>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut dolore magna aliqua.
                </p>
                <h5>Job Nature: Full time</h5>
                <p class="address"><span class="lnr lnr-map"></span> 56/8, Panthapath Dhanmondi Dhaka</p>
                <p class="address"><span class="lnr lnr-database"></span> 15k - 25k</p>
                <a href="#" class="btns text-uppercase">Apply job</a>
            </div>
            <div class="single-rated">
                <img class="img-fluid" src="{{asset('templates/img/r1.jpg')}}" alt="">
                <a href="single.html"><h4>Creative Art Designer</h4></a>
                <h6>Premium Labels Limited</h6>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut dolore magna aliqua.
                </p>
                <h5>Job Nature: Full time</h5>
                <p class="address"><span class="lnr lnr-map"></span> 56/8, Panthapath Dhanmondi Dhaka</p>
                <p class="address"><span class="lnr lnr-database"></span> 15k - 25k</p>
                <a href="#" class="btns text-uppercase">Apply job</a>
            </div>
            <div class="single-rated">
                <img class="img-fluid" src="{{asset('templates/img/r1.jpg')}}" alt="">
                <a href="single.html"><h4>Creative Art Designer</h4></a>
                <h6>Premium Labels Limited</h6>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt ut dolore magna aliqua.
                </p>
                <h5>Job Nature: Full time</h5>
                <p class="address"><span class="lnr lnr-map"></span> 56/8, Panthapath Dhanmondi Dhaka</p>
                <p class="address"><span class="lnr lnr-database"></span> 15k - 25k</p>
                <a href="#" class="btns text-uppercase">Apply job</a>
            </div>
        </div>
    </div>

    <div class="single-slidebar">
        <h4>@lang('front.prestateur_service')</h4>
        <ul class="cat-list">
            @foreach ($catSide as $ca)
                <li><a class="justify-content-between d-flex" href="{{ url('/services/'.$ca->id) }}"><p>{{ $ca->getTranslatedAttribute('title') }}</p><span>{{$ca->services_count}}</span></a></li>
            @endforeach
        </ul>
    </div>
</div>

<!DOCTYPE html>
<html lang="@if(App::getLocale()){{App::getLocale()}}@else{{'fr'}}@endif" @if(App::getLocale() === 'ar') dir="rtl" @endif>
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="name" content="Polyservice253">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>{{ $title }}</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="{{asset('templates/css/linearicons.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/font-awesome.min.css')}}">
			<link rel="stylesheet" href="{{asset('templates/bootstrap/css/bootstrap.min.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/magnific-popup.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/nice-select.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/animate.min.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/owl.carousel.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/main.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/style.css')}}">
			<link rel="stylesheet" href="{{asset('templates/css/bootstrap-select.min.css')}}">

		</head>
		<body>
			  <header id="header" id="home">
			    <div class="container">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="{{url('/')}}" style="color:white"><img src="{{asset('templates/img/icon/logo.png')}}" style="width:70px" alt="" title="" /> PolyServices253</a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li class="menu-active"><a href="{{url('/')}}">@lang('front.menu_1')</a></li>
				          <li><a href="{{url('/services')}}">@lang('front.menu_2')</a></li>
				          <li><a href="{{url('/articles')}}">@lang('front.menu_3')</a></li>
				          <li><a href="{{url('/contact')}}">@lang('front.menu_4')</a></li>
				          <li><a class="ticker-btn" href="{{url('/providers')}}" style="color: black;">@lang('front.prestateur')</a></li>
						  <li><a class="ticker-btn" href="{{url('/customers')}}" style="color: black;">@lang('front.client')</a></li>
				          <li>
						  {{-- language --}}
                            <select id='langSelector' class="selectpicker genric-btn info-border radius medium" data-width="fit" onchange="location = this.value;" style="padding: 6px 20px;">
                                <option  data-content='Français' value=" {{url('/lang/fr')}}" @if(App::getLocale() === 'fr') selected @endif>Français</option>
                                <option data-content='English' value=" {{url('/lang/en')}} " @if(App::getLocale() === 'en') selected @endif>English</option>
                            </select>
                          </li>
                        </ul>
				      </nav><!-- #nav-menu-container -->
			    	</div>
			    </div>
			  </header><!-- #header -->

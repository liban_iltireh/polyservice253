@include('includes.header')

<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    {{ $title }}
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">
            <div class="col-lg-8 post-list">
                @foreach($service->sellers()->get() as $row)
                    <div class="single-post d-flex flex-row">
                        <div class="thumb">
                            <img src="{{asset('storage/'.$row->avatar)}}" height="100" alt="{{ $row->name }}">
                            @if($row->is_premium)
                                <ul class="tags">
                                    <li><img src="{{ asset('templates/img/icon/premium.png') }}" alt="premium" style="width: 55px; height: 48px;"></li>
                                </ul>
                            @endif
                        </div>
                        <div class="details">
                            <div class="title d-flex flex-row justify-content-between">
                                <div class="titles">
                                    <a href="{{ url('/providers/'.$row->id) }}"><h4>{{ $row->name }}</h4></a>
                                </div>
                                <ul class="btns">
                                    <li><a href="#">@lang('front.apply')</a></li>
                                </ul>
                            </div>
                            <div>
                                <p class="address"><span class="lnr lnr-map"></span> {{ $row->region }}</p>
                                <p class="address"><span class="lnr lnr-map-marker"></span> {{ $row->adresse }}</p>
                                <p class="tel"><span class="lnr lnr-phone"></span> {{ $row->tel }}</p>
                            </div>
                            {{--                        <p>{{ $row->getTranslatedAttribute('description') }}</p>--}}
                        </div>
                    </div>

                @endforeach
            </div>
            @include('includes.pagedroit')
        </div>
    </div>
</section>
<!-- End post Area -->
@include('includes.footer')

@include('includes.header')


<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    @lang('front.titre_articles')
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->


<!-- Start blog-posts Area -->
<section class="blog-posts-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 post-list blog-post-list">
                @foreach ($articles as $art)
                    <div class="single-post">
                        <img class="img-fluid" src="{{Voyager::image($art->image)}}" alt="" style="height: 500px;">
                        <a href="{{ url('/blog/' . $art->getTranslatedAttribute('slug')) }}">
                            <h1>
                                {{ $art->getTranslatedAttribute('title') }}
                            </h1>
                        </a>
                        <p>
                            <span class="lnr lnr-calendar-full"></span> {{ $art->date->isoFormat('L') }}
                        </p>
                        <p>
                            {!! Str::limit(strip_tags($art->getTranslatedAttribute('body')), 250, '...') !!}
                        </p>
                    </div>
                @endforeach
                {{ $articles->links() }}
            </div>
            @include('includes.pagedroit')
        </div>
    </div>
</section>
<!-- End blog-posts Area -->


@include('includes.footer')

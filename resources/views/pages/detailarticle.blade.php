@include('includes.header')
<!-- Start blog-posts Area -->
<section class="blog-posts-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 post-list blog-post-list">
                <div class="single-post">
                    <div>
                        <h1>{{ $art->getTranslatedAttribute('title') }}</h1>
                    </div>

                    <div style="text-align: center; margin: 10px 0 30px;">

                        <img class="img-fluid" src="{{ Voyager::image($art->image) }}" style="width: 80%; height: 450px;">
                    </div>
                    <div>
                        <span class="lnr lnr-calendar-full"></span> {{ $art->date->isoFormat('L') }}
                    </div>
                    <div class="content-wrap">
                       <p>
                           {!! $art->getTranslatedAttribute('body') !!}
                       </p>
                    </div>
                    <div class="bottom-meta">
                        <div class="user-details row align-items-center">
                            <div class="social-wrap col-lg-12">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End blog-posts Area -->

@include('includes.footer')

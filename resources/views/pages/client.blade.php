@include('includes.header')
<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Client
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<!-- Start contact-page Area -->
<section class="contact-page-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div style="margin: 20px 0;"><h4>Vous n'avez pas un compte? Inscrivez-vous dès maintenant!</h4></div>
                <div>
                <form class="form-area" action="{{ route('customers.register') }}" method="post">
                    <!--<h4>Inscription</h4>-->
                    @csrf
                    <input name="name" placeholder="Votre nom complet..." class="common-input mb-20 form-control"
                           required="" type="text" value="{{ old('name') }}">


                    <input name="email" placeholder="Votre adresse éléctronique..."
                           pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                           class="common-input mb-20 form-control" required="" type="email" value="{{ old('email') }}">

                    <input name="password" placeholder="Votre mot de passe..." class="common-input mb-20 form-control"
                           required="" type="password">

                    <input name="password_confirmation" placeholder="Confirmation du mot de passe..."
                           class="common-input mb-20 form-control"
                           required="" type="password">

                    <input name="tel" placeholder="Votre numéro de téléphone..." class="common-input mb-20 form-control"
                           required="" type="number" value="{{ old('tel') }}">

                    <div class="input-group-icon mt-10" style="border: 1px solid #ced4da;margin-bottom: 20px;">
                        <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                        <div class="form-select" >
                            <select name="region" id="adresse" class="form-control default-select" required>
                                <option selected disabled default="true">Saisissez la region</option>
                                <option value="Djibouti" @if(old('region') === 'Djibouti') selected @endif >Djibouti</option>
                                <option value="Dikhil" @if(old('region') === 'Dikhil') selected @endif >Dikhil</option>
                                <option value="Tadjourah" @if(old('region') === 'Tadjourah') selected @endif >Tadjourah</option>
                                <option value="Arta" @if(old('region') === 'Arta') selected @endif >Arta</option>
                                <option value="Obock" @if(old('region') === 'Obock') selected @endif >Obock</option>
                                <option value="Ali Sabieh" @if(old('region') === 'Ali Sabieh') selected @endif >Ali Sabieh</option>
                            </select>
                        </div>
                    </div>

                    <input name="adresse" placeholder="Votre adresse..." class="common-input mb-20 form-control"
                           required="" type="text" value="{{ old('adresse') }}">

                    <div class="input-group-icon mt-10" style="border: 1px solid #ced4da;margin-bottom: 20px;">
                        <div class="icon"><i class="fa fa-venus-mars" aria-hidden="true"></i></div>
                        <div class="form-select">
                            <select name="genre" id="genre" class="form-control default-select" required>
                                <option selected disabled default="true">Sexe</option>
                                <option value="Homme" @if(old('genre') === 'Homme') selected @endif >Homme</option>
                                <option value="Femme" @if(old('genre') === 'Femme') selected @endif>Femme</option>
                            </select>
                        </div>
                    </div>
                    <button class="primary-btn mt-20 text-white" style="float: left;" type="reset">Annuler</button>
                    <button class="primary-btn mt-20 text-white" style="float: right;" type="submit">Envoyer</button>
                </form>
                </div>
                <div class="clearfix"></div>
                @if ($errors->any())
                    <div class="mt-20 alert-msg" style="text-align: left;">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @if (session('stored'))
                    <div class="mt-20 alert-msg" style="text-align: left;">
                        <div class="alert alert-success">
                            <span>Votre compte a été crée. Merci de vous connecter.</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-lg-6">
                <div style="margin: 20px 0;"><h4>Vous avez un compte veuillez vous connectez</h4></div>
                <form class="form-area " action="{{ route('customers.login') }}" method="post"
                      class="contact-form text-right">
                    @csrf
                    <input name="email" placeholder="Votre adresse éléctronique..."
                           pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                           class="common-input mb-20 form-control" required="" type="email">

                    <input name="password" placeholder="Votre mot de passe..." class="common-input mb-20 form-control"
                           required="" type="password">
                    <button class="primary-btn mt-20 text-white" style="float: right;">Connexion</button>
                    <div class="mt-20 alert-msg" style="text-align: left;"></div>
                </form>
                @if($errors->first('login'))
                    <div style="color: red; margin-top: 30px;">
                        <span> {{ $errors->first('login') }}</span>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- End contact-page Area -->
@include('includes.footer')

@include('includes.header')

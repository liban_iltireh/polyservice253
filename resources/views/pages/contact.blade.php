@include('includes.header')
<!-- start banner Area -->
<section class="banner-area relative" id="home">	
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    @lang('front.titre_contact') 				
                </h1>
            </div>											
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start contact-page Area -->
<section class="contact-page-area section-gap">
    <div class="container">
        <div class="row">
            <div class="map-wrap" style="width:100%; height: 445px;" id="map"></div>
            <div class="col-lg-4 d-flex flex-column">
                <a class="contact-btns" href="#">@lang('front.titre_cv')</a>
                <a class="contact-btns" href="#">@lang('front.titre_pres')</a>
                <a class="contact-btns" href="#">@lang('front.creer_compte')</a>
            </div>
            <div class="col-lg-8">
                <form class="form-area " id="myForm" action="#" method="post" class="contact-form text-right">
                    <div class="row">	
                        <div class="col-lg-12 form-group">
                            <input name="name" placeholder="@lang('front.contact_nom')" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mb-20 form-control" required="" type="text">
                        
                            <input name="email" placeholder="@lang('front.contact_email')" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mb-20 form-control" required="" type="email">

                            <input name="subject" placeholder="@lang('front.contact_sujet')" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your subject'" class="common-input mb-20 form-control" required="" type="text">
                            <textarea class="common-textarea mt-10 form-control" name="message" placeholder="@lang('front.contact_message')" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                            <button class="primary-btn mt-20 text-white" style="float: right;">@lang('front.contact_bouton')</button>
                            <div class="mt-20 alert-msg" style="text-align: left;"></div>
                        </div>
                    </div>
                </form>	
            </div>
        </div>
    </div>	
</section>
<!-- End contact-page Area -->
@include('includes.footer')
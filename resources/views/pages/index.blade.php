@include('includes.header')
<!-- start banner Area -->
<section class="banner-area relative" id="home" style="margin-top: 85px;">
    <div class="overlay overlay-bg"></div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        </ol>
        <div class="carousel-inner">
            @foreach($slider as $slide)
                @if($loop->first)
                    <div class="carousel-item active">
                        <img src="{{ Voyager::image($slide->image) }}" class="d-block w-100" style="height: 750px;">
                    </div>
                @else
                    <div class="carousel-item">
                        <img src="{{ Voyager::image($slide->image) }}" class="d-block w-100" style="height: 750px;">
                    </div>
                @endif
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</section>
<!-- End banner Area -->
<!-- Start popular-post Area -->
<section class="popular-post-area pt-100">
    <div class="container">
        <div class="row align-items-center">
            <div class="active-popular-post-carusel">
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
                <div class="single-popular-post d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/p1.png" alt="">
                        <a class="btns text-uppercase" href="#">@lang('front.bouton_voir')</a>
                    </div>
                    <a href="#"><h4>Designer</h4></a>
                    <div class="details">
                        <h6>Djibouti-ville</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam quis.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End popular-post Area -->
<!-- Start feature-cat Area -->
<section class="feature-cat-area pt-100" id="category">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-10">
                <div class="title text-center">
                    <h1 class="mb-10">@lang('front.services')</h1>
                    <p>@lang('front.description_servies')</p>
                </div>
            </div>
        </div>
        @foreach($cat as $chunk)
            <div class="row">
                @foreach ($chunk as $ca)
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="single-fcat">
                            <a href="{{ url('/services/'.$ca->id) }}">
                                <img src="{{ url('storage/'.$ca->image) }}" alt="" style="width:100px">
                            </a>
                            <p>{{ $ca->getTranslatedAttribute('title') }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
</section>
<!-- End feature-cat Area -->
<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">
            <div class="col-lg-8 post-list">
                <ul class="cat-list">
                    <li><a href="#">Récent</a></li>
                    <li><a href="#">Full time</a></li>
                    <li><a href="#">Intern</a></li>
                    <li><a href="#">part Time</a></li>
                </ul>
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{asset('templates/img/post.png')}}" alt="">
                        <ul class="tags">
                            <li>
                                <a href="#">Service</a>
                            </li>
                            <li>
                                <a href="#">Sous-service</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="single.html"><h4>TItre de prestation</h4></a>
                                <h6>Type de prestation(particulier ou entreprise)</h6>
                            </div>
                            <ul class="btns">
                                <li><a href="#"><span class="lnr lnr-heart"></span></a></li>
                                <li><a href="#">@lang('front.bouton_postuler')</a></li>
                            </ul>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut dolore magna aliqua.
                        </p>
                        <p class="address"><span class="lnr lnr-map"></span> Djibouti, Héron</p>
                        <p class="address"><span class="lnr lnr-database"></span> Prix : 200 000fjd</p>
                    </div>
                </div>
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{asset('templates/img/post.png')}}" alt="">
                        <ul class="tags">
                            <li>
                                <a href="#">Service</a>
                            </li>
                            <li>
                                <a href="#">Sous-service</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="single.html"><h4>TItre de prestation</h4></a>
                                <h6>Type de prestation(particulier ou entreprise)</h6>
                            </div>
                            <ul class="btns">
                                <li><a href="#"><span class="lnr lnr-heart"></span></a></li>
                                <li><a href="#">@lang('front.bouton_postuler')</a></li>
                            </ul>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut dolore magna aliqua.
                        </p>
                        <p class="address"><span class="lnr lnr-map"></span> Djibouti, Héron</p>
                        <p class="address"><span class="lnr lnr-database"></span> Prix : 200 000fjd</p>
                    </div>
                </div>
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{asset('templates/img/post.png')}}" alt="">
                        <ul class="tags">
                            <li>
                                <a href="#">Service</a>
                            </li>
                            <li>
                                <a href="#">Sous-service</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="single.html"><h4>TItre de prestation</h4></a>
                                <h6>Type de prestation(particulier ou entreprise)</h6>
                            </div>
                            <ul class="btns">
                                <li><a href="#"><span class="lnr lnr-heart"></span></a></li>
                                <li><a href="#">@lang('front.bouton_postuler')</a></li>
                            </ul>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut dolore magna aliqua.
                        </p>
                        <p class="address"><span class="lnr lnr-map"></span> Djibouti, Héron</p>
                        <p class="address"><span class="lnr lnr-database"></span> Prix : 200 000fjd</p>
                    </div>
                </div>
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{asset('templates/img/post.png')}}" alt="">
                        <ul class="tags">
                            <li>
                                <a href="#">Service</a>
                            </li>
                            <li>
                                <a href="#">Sous-service</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="single.html"><h4>TItre de prestation</h4></a>
                                <h6>Type de prestation(particulier ou entreprise)</h6>
                            </div>
                            <ul class="btns">
                                <li><a href="#"><span class="lnr lnr-heart"></span></a></li>
                                <li><a href="#">@lang('front.bouton_postuler')</a></li>
                            </ul>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut dolore magna aliqua.
                        </p>
                        <p class="address"><span class="lnr lnr-map"></span> Djibouti, Héron</p>
                        <p class="address"><span class="lnr lnr-database"></span> Prix : 200 000fjd</p>
                    </div>
                </div>
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{asset('templates/img/post.png')}}" alt="">
                        <ul class="tags">
                            <li>
                                <a href="#">Service</a>
                            </li>
                            <li>
                                <a href="#">Sous-service</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="single.html"><h4>TItre de prestation</h4></a>
                                <h6>Type de prestation(particulier ou entreprise)</h6>
                            </div>
                            <ul class="btns">
                                <li><a href="#"><span class="lnr lnr-heart"></span></a></li>
                                <li><a href="#">@lang('front.bouton_postuler')</a></li>
                            </ul>
                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporinc ididunt
                            ut dolore magna aliqua.
                        </p>
                        <p class="address"><span class="lnr lnr-map"></span> Djibouti, Héron</p>
                        <p class="address"><span class="lnr lnr-database"></span> Prix : 200 000fjd</p>
                    </div>
                </div>
                <a class="text-uppercase loadmore-btn mx-auto d-block"
                   href="category.html">@lang('front.voir_emploi')</a>
            </div>
            @include('includes.pagedroit')
        </div>
    </div>
</section>
<!-- End post Area -->
<!-- Start callto-action Area -->
<section class="callto-action-area section-gap" id="join">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10 text-white">@lang('front.rejoins')</h1>
                    <p class="text-white">@lang('front.description_rejoins')</p>
                    <a class="primary-btn" href="{{url('/client')}}" style="color:black">@lang('front.clients')</a>
                    <a class="primary-btn" href="{{url('/prestataire')}}"
                       style="color:black">@lang('front.prestataire')</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End calto-action Area -->
<!-- Start service Area -->
<section class="service-area section-gap" id="service">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 pb-40 header-text">
                <h1>@lang('front.blog_index')</h1>
            </div>
        </div>
        <div class="row col-md-12">
            @foreach($arts as $art)
                <div class="single-rated col-md-4">
                    <img class="img-fluid" src="{{Voyager::image($art->thumbnail('cropped'))}}" height="450" alt="">
                    <a href="{{ url('/blog/' . $art->getTranslatedAttribute('slug')) }}"><h4
                            style="margin: 10px 0;">{{ $art->getTranslatedAttribute('title') }}</h4></a>
                    <span class="lnr lnr-calendar-full"></span> {{ $art->date->isoFormat('L') }}
                    <p>{!! Str::limit(strip_tags($art->getTranslatedAttribute('body')), 250, '...') !!}</p>
                </div>
            @endforeach
        </div>
    </div>
</section>
<!-- End service Area -->
<!-- Start testimonial Area -->
<section class="testimonial-area section-gap" id="review" style="background-color: #f9f9ff;">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">@lang('front.temoignage')</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="active-review-carusel">
                @foreach($reviews as $rev)
                    <div class="single-review">
                        <div class="title d-flex flex-row">
                            <h4>{{ $rev->name }}</h4>
                        </div>
                        <p>{{ $rev->getTranslatedAttribute('message') }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- End testimonial Area -->
@include('includes.footer')

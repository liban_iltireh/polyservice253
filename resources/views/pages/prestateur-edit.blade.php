@include('includes.header')
<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                   Profil
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<!-- Start contact-page Area -->
<section class="contact-page-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                @if (session('stored'))
                    <div class="mt-20 alert-msg" style="text-align: left;">
                        <div class="alert alert-success">
                            <span>Votre compte a été crée. Merci de conatcter pour vous acquiter des frais et activer votre compte.</span>
                        </div>
                    </div>
                @endif
                <div style="margin: 20px 0;"><h4>Modifer les informations de votre compte</h4></div>
                <div>
                    <form class="form-area" action="{{ route('providers.update') }}" method="post">
                        <!--<h4>Inscription</h4>-->
                        @csrf
                        <input name="name" placeholder="Votre raison sociale ou nom ..." class="common-input mb-20 form-control"
                               required="" type="text" value="{{ $user->name }}">


                        <span class="common-input mb-20 form-control" style="background-color: #6c757d2b;">{{ $user->email }}</span>

                        <input name="tel" placeholder="Votre numéro de téléphone..." class="common-input mb-20 form-control"
                               required="" type="number" value="{{ $user->tel }}">

                        <div class="input-group-icon mt-10"  style="border: 1px solid #ced4da;margin-bottom: 20px;">
                            <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                            <div class="form-select">
                                <select name="region" class="form-control default-select" required>
                                    <option>Saisissez la region</option>
                                    <option value="Djibouti" @if($user->region === 'Djibouti') selected @endif >Djibouti</option>
                                    <option value="Dikhil" @if($user->region === 'Dikhil') selected @endif >Dikhil</option>
                                    <option value="Tadjourah" @if($user->region === 'Tadjourah') selected @endif >Tadjourah</option>
                                    <option value="Arta" @if($user->region === 'Arta') selected @endif >Arta</option>
                                    <option value="Obock" @if($user->region === 'Obock') selected @endif >Obock</option>
                                    <option value="Ali Sabieh" @if($user->region === 'Ali Sabieh') selected @endif >Ali Sabieh</option>
                                </select>
                            </div>
                        </div>

                        <input name="adresse" placeholder="Votre adresse..." class="common-input mb-20 form-control"
                               required="" type="text" value="{{ $user->adresse }}">

                        <div class="input-group-icon mt-10"  style="border: 1px solid #ced4da;margin-bottom: 20px;">
                            <div class="form-select">
                                <select id="ss-multiple" name="services[]" class="selectpicker form-control"
                                        data-live-search="true" title="@lang('front.ss-title')..." required multiple>
                                    @foreach($catSide as $fcat)
                                        <optgroup label="{{ $fcat->title }}">
                                            @foreach($fcat->services as $serv)
                                                <option value="{{ $serv->id }}" @if(isset($fcat->services) && $user->services->contains($serv->id)) selected @endif>
                                                    {{ $serv->getTranslatedAttribute('title') }}</option>
                                            @endforeach
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="primary-btn mt-20 text-white" style="float: left;" type="reset">Annuler</button>
                        <button class="primary-btn mt-20 text-white" style="float: right;" type="submit">Envoyer</button>
                    </form>
                </div>
                <div class="clearfix"></div>
                @if ($errors->any())
                    <div class="mt-20 alert-msg" style="text-align: left;">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-lg-6">
                <div style="margin: 20px 0;"><h4>Vous avez un compte veuillez vous connectez</h4></div>
                <form class="form-area " action="{{ route('providers.login') }}" method="post"
                      class="contact-form text-right">
                    @csrf
                    <input name="password" placeholder="Votre mot de passe..." class="common-input mb-20 form-control"
                           required="" type="password" autocomplete="off">

                    <input name="password_confirmation" placeholder="Confirmation du mot de passe..."
                           class="common-input mb-20 form-control"
                           required="" type="password" autocomplete="off">
                    <button class="primary-btn mt-20 text-white" style="float: right;" type="submit">Connexion</button>
                    <div class="mt-20 alert-msg" style="text-align: left;"></div>
                </form>
                @if($errors->first('login'))
                    <div style="color: red; margin-top: 30px;">
                        <span> {{ $errors->first('login') }}</span>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- End contact-page Area -->
@include('includes.footer')

@include('includes.header')

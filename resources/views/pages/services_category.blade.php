@include('includes.header')

<!-- start banner Area -->
<section class="banner-area relative" id="home">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    {{ $CatServ->getTranslatedAttribute('title') }}
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<!-- Start post Area -->
<section class="post-area section-gap">
    <div class="container">
        <div class="row justify-content-center d-flex">
            <div class="col-lg-8 post-list">
                @foreach($CatServ->services as $row)
                <div class="single-post d-flex flex-row">
                    <div class="thumb">
                        <img src="{{Voyager::image($row->image)}}" height="100" alt="{{ $row->getTranslatedAttribute('title') }}">
                        <ul class="tags">
                            <li>
                                <a href="#">{{ $CatServ->getTranslatedAttribute('title') }}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="details">
                        <div class="title d-flex flex-row justify-content-between">
                            <div class="titles">
                                <a href="{{ url('/services/'.$row->id.'/providers') }}"><h4>{{ $row->getTranslatedAttribute('title') }}</h4></a>
                            </div>
                        </div>
                        <p>{{ $row->getTranslatedAttribute('description') }}</p>
                    </div>
                </div>

            @endforeach
            </div>
            @include('includes.pagedroit')
        </div>
    </div>
</section>
<!-- End post Area -->
@include('includes.footer')

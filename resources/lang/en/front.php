<?php

return [
    /*
    |--------------------------------------------------------------------------
    |Front
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'menu_1' => 'Home',
    'menu_2' => 'Services',
    'menu_3' => 'Blog',
    'menu_4' => 'Contact',
    'services' => 'Different categories',
    'description_servies' => 'The list of the different services offered by PolyServices253.',
    'prestateur_local' => 'Service by location',
    'meilleure_prestateur' => 'Premium provider',
    'prestateur_service' => 'Services By Category',
    'voir_emploi' => 'SEE MORE PROVIDERS',
    'rejoins' => 'Join us today without any hesitation',
    'description_rejoins' => 'We put the service requester in touch with the provider. Please register today don\'t waste your time.',
    'clients' => 'I am a client',
    'prestataire' => 'I am a provider',
    'blog_index' => 'Our blog',
    'temoignage' => 'Testimonials from our clients',
    'bouton_postuler' => 'APPLY',
    'bouton_voir' => 'See the post',
    'titre_service' => 'Service provision',
    'titre_articles' => 'Articles',
    'titre_contact' => 'Contact us',
    'titre_cv' => 'Submit your CV',
    'titre_pres' => 'Publish a service',
    'creer_compte' => 'Create a new account',
    'contact_nom' => 'Your name...',
    'contact_email' => 'Your email address...',
    'contact_sujet' => 'Your subject ...',
    'contact_message' => 'Your message...',
    'contact_bouton' => 'Send',
    'prestateur' => 'Seller',
    'client' => 'CUSTOMER',
    'titre_sous_services' => 'Services',
    'footer_copyright' => 'Copyright',
    'footer_copyright2' => 'Polyservices253. All rights reserved.',
    'ss-title' => 'select the service(s) you offer',
    'apply' => 'Apply'
];

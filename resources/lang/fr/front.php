<?php

return [
    /*
    |--------------------------------------------------------------------------
    |Front
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'menu_1' => 'Accueil',
    'menu_2' => 'Services',
    'menu_3' => 'Blog',
    'menu_4' => 'Contact',
    'services' => 'Différents catégories',
    'description_servies' => 'La liste des différents services proposer par PolyServices253.',
    'prestateur_local' => 'Prestation par localisation',
    'meilleure_prestateur' => 'Prestataire premium',
    'prestateur_service' => 'Services par catégorie',
    'voir_emploi' => 'VOIR PLUS DE PRESTATAIRES',
    'rejoins' => 'Rejoignez-nous aujourd\'hui sans aucune hésitation',
    'description_rejoins' => 'Nous mettons en relation entre le demandeur de service et le prestataire. Veuillez vous s\'inscrire dès aujourd\'hui ne perd pas votre temps.',
    'clients' => 'Je suis un client',
    'prestataire' => 'Je suis un prestataire',
    'blog_index' => 'Notre blog',
    'temoignage' => 'Témoignage de nos clients',
    'bouton_postuler' => 'POSTULER',
    'bouton_voir' => 'voir le poste',
    'titre_service' => 'Prestation de service',
    'titre_articles' => 'Tous les articles',
    'titre_contact' => 'Contactez-nous',
    'titre_cv' => 'Soumettez votre CV',
    'titre_pres' => 'Publier une nouvelle prestation',
    'creer_compte' => 'Créer un nouveau compte',
    'contact_nom' => 'Votre nom...',
    'contact_email' => 'Votre adresse éléctronique...',
    'contact_sujet' => 'Votre sujet...',
    'contact_message' => 'Votre message...',
    'contact_bouton' => 'Envoyer',
    'prestateur' => 'PRESTATAIRE',
    'client' => 'CLIENT',
    'titre_sous_services' => 'Services',
    'footer_copyright' => 'Copyright',
    'footer_copyright2' => 'Polyservices253. Tous droits réservés. ',
    'ss-title' => 'sélectionnez le(s) service(s) que vous proposez',
    'apply' => 'Postuler'

];

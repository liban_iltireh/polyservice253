<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Home::class, 'index']);
Route::get('/contact', [\App\Http\Controllers\Contact::class, 'index']);
Route::get('/customers', [\App\Http\Controllers\Client::class, 'index']);
Route::get('/providers', [\App\Http\Controllers\Prestateur::class, 'index']);
Route::get('/blog/{slug}', [\App\Http\Controllers\Blog::class, 'index']);
Route::get('/services/{slug}', [\App\Http\Controllers\Services::class, 'show']);
Route::get('/services/{slug}/providers', [\App\Http\Controllers\Services::class, 'getSellers']);
Route::get('/articles', [\App\Http\Controllers\Blog::class, 'allArticles']);
Route::get('/services', [\App\Http\Controllers\Services::class, 'index']);

Route::get('/lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'fr'])) {
        abort(403);
    }
    session(['locale' => $locale]);
    return redirect()->back();
});

Route::group(['prefix' => 'customers'], function () {
    Route::post('/login', [\App\Http\Controllers\LoginController::class, 'authenticate'])->name('customers.login');
    Route::get('/logout', [\App\Http\Controllers\LoginController::class, 'logout'])->name('customers.logout');
    Route::post('/register', [\App\Http\Controllers\LoginController::class, 'store'])->name('customers.register');
});

Route::group(['prefix' => 'providers'], function () {
    Route::post('/login', [\App\Http\Controllers\LoginController::class, 'authenticate'])->name('providers.login');
    Route::get('/logout', [\App\Http\Controllers\LoginController::class, 'logout'])->name('providers.logout');
    Route::post('/register', [\App\Http\Controllers\LoginController::class, 'store'])->name('providers.register');

    Route::middleware('auth:seller')->group(function (){
        Route::get('/edit', [\App\Http\Controllers\Prestateur::class, 'edit'])
            ->name('providers.edit');
        Route::post('/update', [\App\Http\Controllers\Prestateur::class, 'update'])
            ->name('providers.update');
    });

});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
